# Pizza dough 2014

- ~6 1/2 cups bread flour
- 2 teaspoons instant yeast
- 3 1/4 cup lukewarm water
- 4 Tablespoons olive oil
- 2 teaspoons salt
- 3 teaspoons sugar

- 1 Large bowl

### Steps

Mix yeast, sugar, 1 cup of flour, and salt together with a cup of water with a whisk in a large bowl. Add remaining flour a half cup at a time, mixing thoroughly, until you get a moist lump of dough. I continue with the whisk until it’s too thick, then switch to a spatula, scraping the edge of the bowl down. 

Liberally dust your counter with flour, and dump the dough in the middle. Kneed or fold dough for around 5-10 minutes. I prefer to fold it over itself on the counter with a dough scraper (instead of my hands), dusting with flour to prevent it from sticking.  The dough should not get tough or dry, your goal is to leave it as wet as possible (and still be able to work with it).  Cut the dough in half, and place in oiled containers (like ice cream buckets). Place in the fridge for a minimum of 45 minutes, and ideally 2-3 days.

Take them out of the fridge an hour prior to cooking. Dust counter with flour, drop dough on counter, dust, and roll into a soft ball. Cut ball into portions (softball sized will do a 15in pizza). Let rise for 30 minutes (or until doubled in size), or while oven pre-heats. This is also a great time to get your toppings ready. Roll or spread out on floured counter. I often use a French rolling pin, alternating directions to keep the pie round. The goal is to stretch the dough slowly enough that it doesn’t rip.
 
Preheat your oven with pizza stone for an hour, and set the temp as high as it will go, though I get consistent results anywhere from 375F - 450F with my pans. Oil your pans (and/or dust with cornmeal). Add sauce and toppings. Thicker crusts will need a lower temperature to cook nicely (375ish), thinner crusts can be cooked more quickly. It will take time for you to determine thickness of crust/toppings to temperature/time. 

Pizzas take 4-10 minutes to cook depending on toppings.

Makes  5-6 15" pizzas.