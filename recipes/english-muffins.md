# English Muffins

1 3/4 cups lukewarm milk
2 1/4 tsp (1 pkg) yeast
3 Tbsp butter, softened
1 1/4 tsp salt
2 Tbsp sugar
1 large egg
4 1/2 cups flour

1. Milk + yeast in bowl, let start and sit for 10 minutes.
2. Add butter, salt, sugar, egg, flour (most of it) and stir with wooden spoon
3. Scrape into oiled bowl and let rise until double
4. Make 55g/2oz balls, press flat, and let rest for 10-20
5. Cook in griddle ~ medium-low, ~10 minutes per side or until golden
6. Cool on rack and split with a fork