I've been trying to replicate the Greek Islands / Rendevous style creamy greek dressings at home. The standard ingredients are simple anyway, and it only takes a few minutes to prepare … so there's no good reason to buy it from the restaurant or the lesser stuff from the grocery store.

The way this works is that I simplify a recipe enough that I can memorize it. Just a ratio or two, a principle, and then I can make it while I prepare dinner (and can make sure we have the ingredients on hand). Greek dressing looks simple enough that it's just 1:1 (oil/acid), and 1:1:1:1 to most of the spices (tsp per 500ml). Important flavours are oregano, garlic, salt, and pepper. It looks like you can stabilize it in the standard ways (lethicin, mustard powder, mayo, or egg yolk), and there are a bunch of other flavours you can fold in.

On to the research ...

Main ratio:

1:1 oil/vinegar (and can split vinegar 50:50 with lemon juice for a more classic flavour)

Per 500ml, flavour with ...

1-2 clove of garlic (top up with some granulated garlic)
2 tsp of oregano (can include some basil)
1.5 tsp of salt
.75tsp of black pepper

Many recipes also include (but I have not tested) ...

1 tsp onion powder
Dijon verus yellow mustard (versus just mustard powder)
1:1 basil to oregano (this seems very basil-y for greek dressing)

A few recipes include ...

Mint (1:1 w/oregano)
Dill (also 1:1)
Thyme (1:1)
Yoghurt (1 tbsp, similar to mayo)  
Marjoram (.25:1)

Extra notes:
Oil/acid ratios varies widely, though 1:1 seems common (least common was .75:1, fairly common was 2:1 … but this tastes very oily)
My preference so far is something around 1:1, splitting the acid between a wine vinegar and lemon (and using a mellower oil)
Mustard powder to help emulsify
(or mustard, though this would push towards more acidy)
Can also cut with 1-2 tbsp mayo to push to creamy (and help emulsify)
(this means  that you could probably whisk into an egg yolk + mustard powder)
Some recipes include water (I don't see the need)

Source recipe samples:


Restaurant:

 1 1/2 quarts olive oil
 1/3 cup garlic powder
 1/3 cup dried oregano
 1/3 cup dried basil
 1/4 cup pepper
 1/4 cup salt
 1/4 cup onion powder
 1/4 cup Dijon-style mustard
 2 quarts red wine vinegar

Smaller version:

3/4 cup oil 1 cup vinegar 2 tsp garlic powder 2 tsp oregano 2 tsp basil 1.5 tsp pepper 1.5 tsp salt 1.5 tsp onion powder 1.5 tsp mustard

Creamy:

Creamy Greek Style Dressing
1/4 cup Extra Virgin Olive Oil
1/4 cup  Fresh lemon juice
1-2 cloves of grated garlic
1 Tbs of honey
2 Tbs of mayonnaise
1-2 tsp of red wine vinegar
salt and black pepper to taste

Creamy 2

1 1/2 quarts olive oil
1/3 cup garlic powder (garlic cloves)
1/3 cup dried oregano
1/3 cup dried basil
1/4 cup pepper
1/4 cup salt
1/4 cup onion powder (onions)
1/4 cup Dijon-style mustard
2 quarts red wine vinegar
(I add some sugar or Honey)
Mix in a blender or food processer

A dry-goods style:

1 cup light virgin olive oil
1 cup red wine vinegar
1 tablespoon dried oregano
1 tablespoon dried basil
1 tablespoon garlic powder
1 tablespoon onion powder
2 teaspoons salt
2 teaspoons pepper
2 teaspoons dijon mustard

A fresher version:

4 TBSP olive oil
3 TBSP lemon juice
2 tsp oregano
1 TBSP crushed garlic
2 tsp red wine vinegar
1/8 tsp kosher salt

A larger ingredient list:

2 large garlic cloves, crushed through a garlic press to get a fine paste
1 tablespoon chopped fresh basil, or two teaspoons dried basil
1 teaspoon salt
1 teaspoon freshly ground black pepper
¼ teaspoon onion powder
1 tablespoon dried Mediterranean or Greek oregano leaves
1 teaspoon white sugar
1 teaspoon chopped fresh mint
Juice of one lemon (about 1/8 cup)
2 tablespoons red wine vinegar
2 tablespoons water
1 teaspoon Dijon mustard
1 cup good quality Greek extra virgin olive oil (use Italian olive oil if you can’t find Greek)

A more general recipe:

1 cup extra virgin olive oil
1/2 cup any vinegar, lemon or lime juice
1/2 cup honey, maple syrup, or jelly
1 heaping Tablespoon Dijon mustard
1 heaping Tablespoon any fresh herb (optional)
a sprinkle or two sea salt and freshly ground pepper








