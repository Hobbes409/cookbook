Vegetarian Jambalaya

- 1 small onion, coarsely chopped
- 1/2 C coarsely chopped celery
- 1 green bell pepper, coarsely chopped
- 1 garlic clove, minced
- One 14.5-oz can diced tomatoes, drained
- 2 T tomato paste
- 1/2 t filé powder (or ocrs, or roux)
- 1 T chopped fresh Italian parsley
- 1 t Tabasco sauce
- 1/2 C water
- 1 1/2 C cooked or canned pinto beans, rinsed and drained if canned
- 4 oz poached tempeh, cut into cubes
- 8 oz frozen vegetarian sausage links, cut into 1-inch pieces
- 4 to 5 C hot cooked long-grain brown rice

### directions

1. In a large pot, heat 1 1/2 t of the oil over medium heat.
2. Add the onion, celery, and bell pepper and sauté for 5 minutes, or until the vegetables begin to soften.
3. Add the garlic, tomatoes, tomato paste, filé powder (if using), parsley, Tabasco, salt, and water.
4. Cover and simmer for 20 minutes, or until the vegetables are soft. Stir in the beans.
5. Meanwhile, heat the remaining 1 1/2 t oil in a large skillet over medium-high heat.
6. Add the tempeh and vegetarian sausage and cook until browned, about 5 minutes.
7. Add the tempeh and sausage to the tomato mixture and simmer for about 10 minutes, until the flavors are well blended.
8. Adjust the seasonings.