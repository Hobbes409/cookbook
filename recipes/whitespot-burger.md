# Whitespot burger doppelganger


This is my take on the classic Whitespot style burger.

The Whitespot burgers are 70:30 beef on a soft bun. I believe they use preformed patties these days, but I’ve found that at home a smashed 4-5oz patty works better as you can get a better crust on them.

### The patty

- 1 package of good, fresh hamburger (large grind), 70:30 (non-lean)
- salt, pepper, and accent
- veg oil (for the pan)

Split beef into 4-5 ounce chunks, season with salt/pepper/accent and form into loose meat balls.  Don’t over-pack the meat balls as you want some irregularity in the patty you will smash on the flat top or cast iron pan later (helps to form a better crust). Set aside while you prepare your other ingredients. Get a pan warmed up (on med/low), which you will turn up to med/high shortly before cooking.

### The rest

Make the mayo, triple-oh sauce, and get your other ingredients together:

- Buns (brioche or soft kaiser)
- Loosely shredded iceberg lettuce
- Sliced tomatoes (something big, red, and fresh)
- Kosher garlic pickles (long slice)
- Cheddar cheese sliced (real if you have it)

Once your ingredients are in place, toast the buns in a medium-heat teflon pan and turn up your grilling pan (medium-high). Add oil to the grilling pan, and drop in a meat ball. Press the meat ball with a spatula (or press if you have one) until it’s ⅛ - ¼ inch thick. Do this as quickly as you can, and then leave the patty alone until you see liquid form on the top. Repeat for each patty, making sure the pan does not cool down.

Flip each patty once a crust has formed. Add cheese shortly before removing from the pan.

The traditional deluxe triple-oh burger goes something like:

```
Bun
   2-3 tbsp of sweetened mayo
   Lettuce
   Fresh tomato slice (or two)
   Kosher pickle (or three)
   Cheese
   Patty
   Tripple oh, lots and lots
Bun
```

Serve with salad or fresh cut fries!

### Whitespot mayo

Makes enough for about 8 burgers.

- 1.5 cups Hellmann’s real mayo
- 1 egg yolk
- 1/2 tsp paprika
- 1 pinch cayenne
- 1 tbsp honey
- 1 tsp cider vinegar

Mix ingredients together with a whisk. Add extra vinegar to taste.

### Tripple "O" Sauce

A bit of research suggests that the original triple oh sauce was a combination of off the shelf products:

- 1 part Bicks Red Relish
- 1 part Heinz Chilli Sauce 

Mix with a spoon.

I’ve found that this is close, but that the ratio may be off (3:2 is tastes better to me).


