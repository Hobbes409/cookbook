# Greek islands style dressing

- 1 cup  Extra virgin olive oil
- 1/4 cup Fresh lemon juice (1-2 lemons)
- 1/2 cup Red wine or cider vinegar

- 1-2 cloves Garlic (shredded or minced)
- 2 tsp Dried oregano
- 1 tsp Dried basil
- 1 dash Accent (MSG) - optional, can replace with a dash of "Braggs liquid aminos"

- 1 tsp Kosher salt
- 1 tsp White sugar
- 1 tsp Fresh ground black pepper
- 1 tsp Garlic powder
- 1 tsp Onion powder

- 1 dash Mustard powder (helps stabilize)
- 1 tsp Dijon mustard (helps stabilize)

Pour the ingredients into a container and shake for 2 minutes. Taste, and adjust the seasoning (the lemon may be more or less sweet, and you may want more or less salt).

----

This dressing isn't quite as thick as the restaurant dressing (but it's close). To thicken, whip 1-2 egg yolks in a bowl with another pinch of mustard powder and drizzle in the above shaken ingredients.

Keeps in the fridge for a week.

### Variations:

- Nearly any tasty vinegar can be used
- The lemon juice can be replaced by the same volume of vinegar
- Onion powder and basil are optional (if you're out)
- Mustard powder/Dijon are optional, but the dressing will separate in the fridge more quickly
