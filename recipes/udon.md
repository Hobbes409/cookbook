# Simple udon noodles

Fresh udon noodles take about ten minutes to prepare if you plan ahead.

### Ingredients:

- 3¼ cups all purpose flour
- 1 tbsp salt (or 1.5)
- 1 cup water

### Method:

1. Mix salt and flour in a large, sturdy mixing bowl, making a well in the centre
2. Add the water slowly, mixing it with the dry ingredients with your fingers
3. Kneed the semi-crumbly dough until it forms a shape. Continue kneading the ball of dough on the counter for 5 minutes, sprinkling flour on the work surface should the ball stick. It should be firm and pliable.
4. Slap it in the fridge in a sealed bag or container for 4-8 hours (or overnight). Longer is better, as it allows the gluten to unfold.
5. Take the dough out of the fridge and let it come up to room temperature (~20 minutes).
6. Roll the dough out to 3-4mm thick and dust on both sides with a roller. Dust with flour and fold the rolled dough, slicing into 3-4mm slices
7. You can stretch the noodles out further if you’d like thinner noodles (or if you cut/roll them too large)
8. The noodles cook quickly (3-4 minutes in salted, boiling water). Add to heated stock and veggies and enjoy!

There are many regional stock and ingredient combinations for the soup, but the photos below show a Nabeyaki udon (more or less). The broth is based on a shrimp stock, fortified with mushroom stock, a few tablespoons of soy, and some cayenne for kick. Vegetables are added to the stock once it hits the boil, starting with the thickest cuts. Cook only until vegetables start to soften as they will continue to cook once added to the bowls. Leafy vegetables and other additions are added after the stock is poured into the bowls.

Add the freshly cooked noodles to the broth using a large strainer spoon, and drop an egg in if you wish (it will cook before it’s cool enough to eat).