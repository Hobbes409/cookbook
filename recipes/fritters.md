## General veggie fritters

- 3-4 cups of shredded veg
- ½ cup flour
- 1 tsp. salt (or to taste)
- 1 egg
- 1/4 cup plus 2 tablespoons scallions, chopped finely
- ½ tsp. baking powder
- Season to taste


### Directions: 

In a large bowl, combine shredded veg, flour, baking powder, salt and egg. Use a fork to combine. Heat 2 tablespoons of olive oil in a skillet over medium high heat. Drop heaping spoonfuls of the mixture onto oil and allow to cook for a few minutes before flipping (4-5 minutes per side). Remove to a towel-lined cooling rack to remove excess oil.

These fritters can take many types of spice mixes (indian, asian, greek, etc.).