# Apple, Berry & Almond Scones

Makes 12 scones

### Ingredients:

- 650g plain flour
- 100g caster sugar
- 20g baking powder
- 1 tsp cinnamon
- Pinch of salt
- 125g cold butter, diced
- 1 large apple (or 2 medium) peeled & diced
- 2 eggs
- 300ml buttermilk
- 2 tsp vanilla extract
- Handful fresh berries (raspberries, blueberries or blackberries)
- Handful flaked almonds
- Icing sugar, to dust

### Method:

1. Preheat the oven to 200°C / 180°C fan assisted.
2. In a large clean bowl combine the flour, sugar, baking powder, salt & cinnamon. Rub the diced butter into the flour using your fingertips until the mix resembles breadcrumbs in texture. Add the diced apple to the flour mixture.
3. Add the eggs, buttermilk and vanilla to the flour, combine all the ingredients together to form a rough dough.
4. Turn the scone mixture out onto a lightly floured work surface and knead gently until all the flour has been combined.
5. Using a rolling pin to roll out the scone mixture to about 1-inch thickness. Cut the scones using a floured cutter and place on a baking tray lined with parchment paper evenly spaced apart.
6. Make an indentation into the centre of each scone using your thumb. Brush the top of each scone with a beaten egg. Stuff the indentation in each scone with 1 to 2 fresh berries of your choice and top with flaked almonds.
7. Place the scones in the preheated oven and bake for approximately 16 to 18 minutes. Half- way through baking, rotate the baking tray in the oven to ensure an even bake.
8. Once baked allow to cool on a wire rack before, then dust with icing sugar and serve with preserves & cream, or better yet, clotted cream.