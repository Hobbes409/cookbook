What do I think about my food site?

I'm not trying to replicate or copy Serious Eats, Good Eats, or any of the other food things we talked about. 

Good eats was interesting. But, it wasn't really very inspiring. Any of Anthony Bourdain's TV shows are inspiring, but they're not that informative. I mean, you can learn from them, but he doesn't often describe how to use the limitations to your advantage. 

We already talked about how the site could look, in the sense that it would need to feel like something about cookery. It would need to feel more inspired than The Muppet show style of Good Eats. But, it wouldn't necessarily need to be a thing of beauty.

What what I write about?

I was reading some articles tonight from Serious Eats. The author was talking about a meatball pizza.  He said something about a pizza steel being the only way to cook a pizza. This statement seemed so dishonest, so close-minded. I want to avoid writing like this. 

On the other hand, I love pizza. I would definitely write about pizza. I would write about various ways to speed up making the dough. Ways in which you could bend the dough toward various flavour profiles. Ways in which you could use the dough for other purposes. Can you be frugal with something like dough? Of course you can.

I would write about pizza sauces, and how I've seen dozens of different base recipes and different approaches for sauces. And how the same is true for various pasta sauces. I would want to graph how a cheese sauce has discreet approaches: The classic cream approach, with no thickeners.  The classic French approach with a roux. The American approach with cornstarch or other thickeners. Or the gross disgusting American approach with prefabricated cheese spreads. Which direction is better the more true? Which is the most comforting? Which can I make with what I have on hand?

I would also want to write about things like making food more comforting, or more healthful, or brighter, or more interesting.  And while I'm not a big fan of deconstruction or molecular gastronomy, that I think there's a place for some of these techniques in our every day food. Turns out many of the foods we eat  and many of food techniques we use are naturally evolved pieces of deconstruction and the more refined micro techniques. One example of this is pizza, which is really a deconstruction and combination of sandwich pasta and meat dish.

I'd want to write about some of the more inspiring standard techniques. There was a tomato sauce recipe I saw, that was nearly hand crushed tomatoes, olive oil, salt, and onions that are removed before serving. There's a lot to learn from some of these more basic, and more inspired classical approaches. 

I think one of the tricks of getting a site like this right, is that you need to separate this theory from the recipes themselves. Though, I think you would want to tie the gradients of recipes and techniques and tools and some graphical. For example you could show the venn diagram or quadrant diagram of cheese sauces. Or you could show them by the time it takes to prepare them, or the skill level, or the ingredients, or the tools.

There are also a lot of things that can be done with interchangeable food components. Something I see people struggling with in their kitchens is how to procure, manage, and effectively use the food they buy. A good example of this is how I decided to make dinner last night. My stomach wanted a sandwich, but my refrigerator insisted on something with mushroom stock. Mushroom stock was what needed to be used first. So there's some philosophy in terms of how to put together these techniques tools and various tidbits.

It is almost as if this is the Zen and art of food hackery. But a title like that would be impossible to live up to. It is also disrespect to Zen. 

Maybe something like this really does need to be more than one website. Or maybe it needs to be a book. Or maybe it needs to be a YouTube channel. I don't know yet, and this is why I haven't done anything.

On another topic, I had a friend recently suggest that I should write a book
about dealing with burnout - as she observed that I have shared a good deal
of relevant practical advice based on my own experience.  I've started
thinking about how to write that stuff down and what format it would present
best in.

A book about burnout is a good idea. I think a book like that could take a number of different forms. You could, for example, write it as lengthy narrative. Or, you could write it as a series of disconnected essays or short stories. 

I've seen a lot of books and essays about burnout. The main problem I've seen is that the authors take themselves too seriously. The writing is often very businesslike, sterile, and uninteresting.  You would want to avoid this boilerplate style. There are already enough books like this.

I think you would also want to avoid the Buzzfeed style of nonsense. This is almost home 37signals writes, in a readers digest style, dumbed down, simplistic format. 

I suspect most ideal book on the topic would be entertaining enough for someone who is burnt out to read. But it would also need to contain the factual information needed to learn something. So perhaps something like classic consultant guides by Weinburg. 

(note: I dictated this on my phone, so there are likely very comical grammar or spelling errors.)